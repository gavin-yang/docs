# OpenHarmony文档

欢迎访问OpenHarmony文档仓库，参与OpenHarmony开发者文档开源项目，与我们一起完善开发者文档。

此仓库存放OpenHarmony网站提供的设备开发、应用开发对应的开发者文档。

## 文档目录结构

[访问官网](https://www.openharmony.cn/)

[中文文档](zh-cn/readme.md)

[English Documentation](en/readme.md)

## OpenHarmony文档版本分支说明

### 最新版本

master：最新开发版本。  

发布OpenHarmony 3.1 Release版本，[了解版本详情](zh-cn/release-notes/OpenHarmony-v3.1-release.md)。

发布OpenHarmony 3.0 LTS版本，[了解版本详情](zh-cn/release-notes/OpenHarmony-v3.0-LTS.md)。该版本已更新至OpenHarmony 3.0.2 LTS，[了解版本详情](zh-cn/release-notes/OpenHarmony-v3.0.2-LTS.md)。

发布 OpenHarmony v2.2 Beta2版本，[了解版本详情](zh-cn/release-notes/OpenHarmony-v2.2-beta2.md)。

发布OpenHarmony 2.0 Canary预览版本，[了解版本详情](zh-cn/release-notes/OpenHarmony-2-0-Canary.md)。

### 历史稳定版本

OpenHarmony_v1.x_release：OpenHarmony 1.1.4 LTS稳定版本，[了解版本详情](zh-cn/release-notes/OpenHarmony-v1-1-4-LTS.md)。

[了解更多版本详情](zh-cn/release-notes/)。


## 第三方开源软件及许可说明

3rd-Party-License：[第三方开源软件及许可证说明](zh-cn/contribute/第三方开源软件及许可证说明.md)

## 贡献

非常欢迎您参与[贡献](zh-cn/contribute/参与贡献.md)，我们鼓励开发者以各种方式参与文档反馈和贡献。

您可以对现有文档进行评价、简单更改、反馈文档质量问题、贡献您的原创内容，详细请参考[贡献文档](zh-cn/contribute/贡献文档.md)。

卓越贡献者将会在开发者社区文档贡献专栏表彰公示。