# Media Resource Types


- The following table describes the image resource types supported by the development framework.
    | Image Format | File Name Extension | 
  | -------- | -------- |
  | JPEG | .jpg | 
  | PNG | .png | 
  | GIF | .gif | 
  | SVG | .svg | 
  | WEBP | .webp | 
  | BMP | .bmp |
