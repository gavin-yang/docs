# Quick Start

-   Getting Started
    -   [Preparations](start-overview.md)
    -   [Getting Started with eTS](start-with-ets.md)
    -   [Getting Started with JavaScript in the Traditional Coding Approach](start-with-js.md)
    -   [Getting Started with JavaScript in the Low-Code Approach](start-with-js-low-code.md)

-   Development Fundamentals
    -   [Directory Structure](package-structure.md)
    -   [Resource File](basic-resource-file-categories.md)

