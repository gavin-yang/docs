# OpenHarmony Documentation

Welcome to the OpenHarmony documentation repository.

This repository stores device and application development documents provided by OpenHarmony. Your contribution to the OpenHarmony documentation will be highly appreciated.

## Contents

[Chinese Documentation](zh-cn/readme.md)

[English Documentation](en/readme.md)

## OpenHarmony Document Version Branches

### Latest Versions

 - master: the latest version.

 - OpenHarmony 3.1 Beta. [Learn more](en/release-notes/OpenHarmony-v3.1-beta.md)

 - OpenHarmony 3.0 LTS. [Learn more](en/release-notes/OpenHarmony-v3.0-LTS.md)

   This version is upgraded to OpenHarmony 3.0.2 LTS. [Learn more](en/release-notes/OpenHarmony-v3.0.2-LTS.md)

 - OpenHarmony 2.2 Beta2. [Learn more](en/release-notes/OpenHarmony-v2.2-beta2.md)

 - OpenHarmony 2.0 Canary. [Learn more](en/release-notes/OpenHarmony-2-0-Canary.md)

### Historical Stable Versions

OpenHarmony_v1.x_release: OpenHarmony v1.1.4 LTS. [Learn more](en/release-notes/OpenHarmony-v1-1-4-LTS.md)

[More versions](en/release-notes/)


## Third-Party Open-Source Software and License Notice

3rd-Party-License: [Third-Party Open-Source Software and License Notice](en/contribute/third-party-open-source-software-and-license-notice.md)

## How to Contribute

A great open-source project wouldn't be possible without the hard work of many contributors. We'd like to invite anyone from around the world to  [participate](contribute/contribution.md)  in this exciting journey, and we're grateful for your time, passion, and efforts!

You can evaluate available documents, make simple modifications, provide feedback on document quality, and contribute your original content. For details, see  [Documentation Contribution](contribute/documentation-contribution.md).

Excellent contributors will be awarded and the contributions will be publicized in the developer community.
