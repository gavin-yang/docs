# 开发参考

-   [基于JS扩展的类Web开发范式](arkui-js/Readme-CN.md)

-   [基于TS扩展的声明式开发范式](arkui-ts/Readme-CN.md)

-   [接口](apis/Readme-CN.md)

