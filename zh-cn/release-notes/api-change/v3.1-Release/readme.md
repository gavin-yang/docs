# Readme

本目录记录了OpenHarmony 3.1 Release版本相较OpenHarmony 3.0 LTS版本的API变化，包括新增、变更、废弃、删除。

- [JS API差异报告](js-apidiff-v3.1-release.md)
- [Native API差异报告](native-apidiff-v3.1-release.md)
